#!/bin/env python3

from xrcon.client import XRcon
rcon = XRcon('127.0.0.1', 26000, 'qweasd123')
rcon.connect()

if __name__ == '__main__':
    while True:
        str_to_send = input()
        try:
            data = rcon.execute('say ' + str_to_send)
        except:
            rcon.connect()
            data = rcon.execute('say ' + str_to_send)
        print(data)


