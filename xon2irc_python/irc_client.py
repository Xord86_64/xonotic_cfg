#!/usr/bin/env python3
# by bl4de | github.com/bl4de | twitter.com/_bl4de | hackerone.com/bl4de
import socket
import threading
import sys
import time


def usage():
    print("IRC simple Python client | by bl4de | github.com/bl4de | twitter.com/_bl4de | hackerone.com/bl4de\n")
    print("$ ./irc_client.py USERNAME CHANNEL\n")
    print("where: USERNAME - your username, CHANNEL - channel you'd like to join (eg. channelname or #channelname)")


def channel(channel):
    if channel.startswith("#") == False:
        return "#" + channel
    return channel

# helper function used as thread target
def print_response():
    resp = client.get_response()
    if resp:
        print(resp)
        msg = resp.strip().split(":")
        #print("< {}> {}".format(msg[1].split("!")[0], msg[2].strip()))


class IRCSimpleClient:

    def __init__(self, username, channel, server="195.133.44.67", port=6667):
        self.username = username
        self.server = server
        self.port = port
        self.channel = channel

    def connect(self):
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn.connect((self.server, self.port))

    def get_response(self):
        return self.conn.recv(512).decode("utf-8")

    def send_cmd(self, cmd, message):
        command = "{} {}\r\n".format(cmd, message).encode("utf-8")
        print("command: " + str(command))
        self.conn.send(command)

    def send_message_to_channel(self, message):
        command = "PRIVMSG {}".format(self.channel)
        message = ":" + message
        print("cmd: " + command + " msg: " + message)
        self.send_cmd(command, message)

    def join_channel(self):
        cmd = "JOIN"
        channel = self.channel
        print("joining: " + cmd + " " + channel)
        self.send_cmd(cmd, channel)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
        exit(0)
    else:
        username = sys.argv[1]
        channel = channel(sys.argv[2])

    cmd = ""
    joined = False
    client = IRCSimpleClient(username, channel)
    client.connect()

    print("Sending client info")
    client.send_cmd("CAP", "LS 302")
    client.send_cmd("NICK", username)
    client.send_cmd(
        "USER", "{} * * :{}".format(username, username))

    while(joined == False):
        resp = client.get_response()
        print(resp.strip())

        if "001" in resp:
            print("Connected")

        # username already in use? try to use username with _
        elif "433" in resp:
            print("username already in use. Trying to add dash")
            username = "_" + username
            client.send_cmd("NICK", username)
            client.send_cmd(
                "USER", "{} * * :{}".format(username, username))

        # we're accepted, now let's join the channel!
        elif "376" in resp or "422" in resp:
            print("Got end of MOTD or noMOTD. Sending JOIN")
            client.join_channel()

        # if PING send PONG with name of the server
        elif "PING" in resp:
            client.send_cmd("PONG", ":" + resp.split(":")[1])

        # we've joined
        elif "366" in resp:
            print("Joined")
            joined = True

        else:
            print("unknown response from server")
            print("sleeping for 1 secs")
            time.sleep(1)


    while(cmd != "/quit"):
        print("main loop")
        cmd = input("< {}> ".format(username)).strip()
        if cmd == "/quit":
            client.send_cmd("QUIT", "Good bye!")
        client.send_message_to_channel(cmd)

        # socket conn.receive blocks the program until a response is received
        # to prevent blocking program execution, receive should be threaded
        response_thread = threading.Thread(target=print_response)
        response_thread.daemon = True
        response_thread.start()
