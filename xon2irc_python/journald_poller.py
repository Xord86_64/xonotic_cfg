#!/bin/env python3

import select
import re

from systemd import journal

j = journal.Reader()
j.log_level(journal.LOG_INFO)

j.add_match(_SYSTEMD_UNIT='xon-ffa-server.service')
j.seek_tail()
j.get_previous()
# j.get_next() # it seems this is not necessary.

p = select.poll()
p.register(j, j.get_events())

connect_pattern = re.compile("^:join:.*")
chat_pattern = re.compile("^:chat:*")
disconnect_pattern = re.compile("^:part:.*")

players = {}

def player_connect (log_str):
    global players
    # msg like ':joint:1:1:IP_ADDR:nickname'
    log_str_elements = re.split(':', log_str, maxsplit=5)
    if log_str_elements[4] != "bot":
        normalized_nickname = re.sub('\\x1b.*?m', '.', log_str_elements[5])
        players.update({log_str_elements[3]: normalized_nickname})
        connect_msg = normalized_nickname + ' ' + "connected"
    else:
        connect_msg = "bot"
    return connect_msg

def player_disconnect (log_str):
    global players
    # msg like ':part:1'
    log_str_elements = re.split(':', log_str)
    if log_str_elements[2] in players:
        disconnect_msg = players[log_str_elements[2]] + ' ' + "disconnected"
        players.pop(log_str_elements[2])
    else:
        disconnect_msg = "bot"
    return disconnect_msg

def player_say (log_str):
    log_str_elements = re.split(':', log_str, maxsplit=3)
    if log_str_elements[2] in players:
        chat_msg = players.get(log_str_elements[2]) + ": " + log_str_elements[3]
    else:
        chat_msg = "fix_me" + ": " + log_str_elements[3]
    return chat_msg

if __name__ == "__main__":
    while p.poll():
        if j.process() != journal.APPEND:
            continue
    
        # Since each iteration of a journal.Reader() object is equal to "get_next()", just do simple iteration.
        for entry in j:
            if entry['MESSAGE'] != "":
                try_match_connect = connect_pattern.match(entry['MESSAGE'])
                try_match_chat = chat_pattern.match(entry['MESSAGE'])
                try_match_disconnect = disconnect_pattern.match(entry['MESSAGE'])
                if type(try_match_connect) is re.Match:
                    formatted = player_connect(entry['MESSAGE'])
                elif type(try_match_disconnect) is re.Match:
                    formatted = player_disconnect(entry['MESSAGE'])
                elif type(try_match_chat) is re.Match:
                    formatted = player_say(entry['MESSAGE'])
                else:
                    continue
                if formatted != "bot":
                    print(formatted)
